package com.tleksono.base.data.source.remote.response


import com.google.gson.annotations.SerializedName
import com.tleksono.base.domain.model.Contact

data class ContactsResponse(
    @SerializedName("info")
    val info: Info?,
    @SerializedName("results")
    val results: List<Result?>?
) {
    data class Info(
        @SerializedName("page")
        val page: Int?,
        @SerializedName("results")
        val results: Int?,
        @SerializedName("seed")
        val seed: String?,
        @SerializedName("version")
        val version: String?
    )

    data class Result(
        @SerializedName("cell")
        val cell: String?,
        @SerializedName("dob")
        val dob: Dob?,
        @SerializedName("email")
        val email: String?,
        @SerializedName("gender")
        val gender: String?,
        @SerializedName("id")
        val id: Id?,
        @SerializedName("location")
        val location: Location?,
        @SerializedName("name")
        val name: Name?,
        @SerializedName("nat")
        val nat: String?,
        @SerializedName("phone")
        val phone: String?,
        @SerializedName("picture")
        val picture: Picture?
    ) {
        data class Dob(
            @SerializedName("age")
            val age: Int?,
            @SerializedName("date")
            val date: String?
        )

        data class Id(
            @SerializedName("name")
            val name: String?,
            @SerializedName("value")
            val value: String?
        )

        data class Location(
            @SerializedName("city")
            val city: String?,
            @SerializedName("coordinates")
            val coordinates: Coordinates?,
            @SerializedName("country")
            val country: String?,
            @SerializedName("postcode")
            val postcode: Any?,
            @SerializedName("state")
            val state: String?,
            @SerializedName("street")
            val street: Street?,
            @SerializedName("timezone")
            val timezone: Timezone?
        ) {
            data class Coordinates(
                @SerializedName("latitude")
                val latitude: String?,
                @SerializedName("longitude")
                val longitude: String?
            )

            data class Street(
                @SerializedName("name")
                val name: String?,
                @SerializedName("number")
                val number: Int?
            )

            data class Timezone(
                @SerializedName("description")
                val description: String?,
                @SerializedName("offset")
                val offset: String?
            )
        }

        data class Name(
            @SerializedName("first")
            val first: String?,
            @SerializedName("last")
            val last: String?,
            @SerializedName("title")
            val title: String?
        )

        data class Picture(
            @SerializedName("large")
            val large: String?,
            @SerializedName("medium")
            val medium: String?,
            @SerializedName("thumbnail")
            val thumbnail: String?
        )

        fun toContact(): Contact {
            return Contact(
                id?.value.orEmpty(),
                name?.first.orEmpty(),
                name?.last.orEmpty(),
                picture?.thumbnail.orEmpty(),
                picture?.large.orEmpty(),
                dob?.date.orEmpty(),
                if (gender == "male") "Pria" else "Wanita",
                email.orEmpty(),
                phone.orEmpty(),
                cell.orEmpty(),
                StringBuilder().append("${location?.street} ")
                    .append("${location?.city} ")
                    .append("${location?.state} ")
                    .append("${location?.postcode} ").toString(),
                location?.coordinates?.latitude.orEmpty(),
                location?.coordinates?.longitude.orEmpty()
            )
        }
    }
}