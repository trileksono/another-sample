package com.tleksono.base.data.repo

import com.tleksono.base.data.source.remote.ApiServices
import com.tleksono.base.domain.model.Contact
import com.tleksono.base.domain.model.UIState
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import kotlinx.coroutines.flow.onStart

interface ContactsRepository {

    suspend fun getContacts(): Flow<UIState<List<Contact?>>>

}

class ContactsDataSource(private val apiServices: ApiServices) : ContactsRepository {
    override suspend fun getContacts(): Flow<UIState<List<Contact?>>> {
        return flow {
            val response = apiServices.getRandomContacts()
            if (response.isSuccessful) {
                emit(UIState.success(response.body()?.results?.map { it?.toContact() }.orEmpty()))
            } else {
                emit(UIState.error<List<Contact?>>("Something wrong"))
            }
        }.onStart {
            emit(UIState.loading())
        }.flowOn(Dispatchers.IO)
    }
}
