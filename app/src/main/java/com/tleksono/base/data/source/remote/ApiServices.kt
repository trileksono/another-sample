package com.tleksono.base.data.source.remote

import com.tleksono.base.data.source.remote.response.ContactsResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiServices {

    @GET("api")
    suspend fun getRandomContacts(
        @Query("result") result: Int = 5,
        @Query("exc") exc: String = "login,registered,i"
    ): Response<ContactsResponse>
}
