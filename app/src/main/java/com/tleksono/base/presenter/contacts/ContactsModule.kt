package com.tleksono.base.presenter.contacts

import com.tleksono.base.data.repo.ContactsDataSource
import com.tleksono.base.data.repo.ContactsRepository
import com.tleksono.base.data.source.remote.ApiServices
import com.tleksono.base.domain.usecase.ContactUseCase
import com.tleksono.base.domain.usecase.ContactsUseCaseImpl
import dagger.Module
import dagger.Provides

@Module
class ContactsModule {

    @Provides
    fun provideDataSource(apiServices: ApiServices): ContactsRepository =
        ContactsDataSource(apiServices)

    @Provides
    fun provideUseCase(repository: ContactsRepository): ContactUseCase {
        return ContactsUseCaseImpl(repository)
    }

}
