package com.tleksono.base.presenter.contacts

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.tleksono.base.presenter.main.MainAdapter
import kotlinx.android.synthetic.main.activity_contacts.*
import javax.inject.Inject

class ContactsActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel by viewModels<ContactViewModel> { viewModelFactory }
    private lateinit var mainAdapter: MainAdapter

    companion object {
        private const val EXTRA_ID = ".extra.ID"

        fun createIntent(caller: Context, id: String): Intent {
            return Intent(caller, ContactsActivity::class.java).apply {
                putExtra(EXTRA_ID, id)
            }
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        recyclerContacts.run {
            layoutManager = LinearLayoutManager(this@ContactsActivity)
        }
    }
}
