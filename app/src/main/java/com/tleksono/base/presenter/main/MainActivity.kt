package com.tleksono.base.presenter.main

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.tleksono.base.R
import com.tleksono.base.domain.model.Contact
import com.tleksono.base.presenter.contacts.ContactsActivity
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    companion object {
        val codeRequestMain = 99
        val resultKey = "Key.Result"
    }

    private lateinit var mainAdapter: MainAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initialize()
    }

    private fun initialize() {
        recyclerMain.run {
            layoutManager = LinearLayoutManager(this@MainActivity)
            mainAdapter = MainAdapter {
                // todo on detail
            }
            adapter = mainAdapter
        }

        imgAdd.setOnClickListener {
            startActivityForResult(
                ContactsActivity.createIntent(this, ""), codeRequestMain
            )
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK && requestCode == codeRequestMain) {
            mainAdapter.addData(data?.getSerializableExtra(resultKey) as Contact)
        }
    }
}