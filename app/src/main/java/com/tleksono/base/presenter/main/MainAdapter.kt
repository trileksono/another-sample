package com.tleksono.base.presenter.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.tleksono.base.R
import com.tleksono.base.domain.model.Contact
import kotlinx.android.synthetic.main.item_contact.view.*

class MainAdapter(
    val models: MutableList<Contact> = mutableListOf(),
    val listener: (Contact) -> Unit
) : RecyclerView.Adapter<MainAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_contact, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.itemView.run {
            Glide.with(this)
                .load(models[position].imgThumbnails)
                .into(imgProfile)

            txtFullName.text = "${models[position].firstName} ".plus(models[position].lastName)

            setOnClickListener { listener(models[position]) }
        }
    }

    fun removePaket(position: Int) {
        models.removeAt(position)
        notifyItemRemoved(position)
        notifyItemRangeChanged(position, models.size)
    }

    fun addData(contact: Contact) {
        models.add(contact)
        notifyItemInserted(models.size)
    }

    override fun getItemCount(): Int = models.size

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view)
}
