package com.tleksono.base.presenter.contacts

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tleksono.base.domain.model.Contact
import com.tleksono.base.domain.model.UIState
import com.tleksono.base.domain.usecase.ContactUseCase
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

class ContactViewModel @Inject constructor(
    val contactUseCase: ContactUseCase
) : ViewModel() {

    private var _contactLiveData = MutableLiveData<UIState<List<Contact>>>()
    val contactLiveData: LiveData<UIState<List<Contact>>>
        get() = _contactLiveData

    init {
        viewModelScope.launch {
            contactUseCase.getContact()
                .collect { _contactLiveData.postValue(it) }
        }
    }
}
