package com.tleksono.base.domain.usecase

import com.tleksono.base.data.repo.ContactsRepository
import com.tleksono.base.domain.model.Contact
import com.tleksono.base.domain.model.UIState
import kotlinx.coroutines.flow.Flow

interface ContactUseCase {
    suspend fun getContact(): Flow<UIState<List<Contact?>>>
}

class ContactsUseCaseImpl(private val contactsRepository: ContactsRepository) : ContactUseCase {
    override suspend fun getContact(): Flow<UIState<List<Contact?>>> {
        return contactsRepository.getContacts()
    }
}
