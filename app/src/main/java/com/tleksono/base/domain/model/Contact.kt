package com.tleksono.base.domain.model

import java.io.Serializable

data class Contact(
    val id: String,
    val firstName: String,
    val lastName: String,
    val imgThumbnails: String,
    val imgFull: String,
    val birthDate: String,
    val gender: String,
    val email: String,
    val phone: String,
    val cell: String,
    val address: String,
    val latitude: String,
    val longatitude: String
) : Serializable
