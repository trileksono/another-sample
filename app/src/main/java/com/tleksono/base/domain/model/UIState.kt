package com.tleksono.base.domain.model

data class UIState<T>(val status: Status, val data: T?, val error: String) {

    enum class Status {
        SUCCESS, ERROR, LOADING
    }

    companion object {
        fun <T> success(data: T): UIState<T> {
            return UIState(Status.SUCCESS, data, "")
        }

        fun <T> error(error: String): UIState<T> {
            return UIState(Status.ERROR, null, error)
        }

        fun <T> loading(): UIState<T> {
            return UIState(Status.LOADING, null, "")
        }
    }
}
