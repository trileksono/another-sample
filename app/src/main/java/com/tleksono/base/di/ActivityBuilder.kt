package com.tleksono.base.di

import com.tleksono.base.presenter.contacts.ContactsActivity
import com.tleksono.base.presenter.contacts.ContactsModule
import com.tleksono.base.presenter.main.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ContributesAndroidInjector()
    abstract fun bindMainActivity(): MainActivity

    @ContributesAndroidInjector(modules = [ContactsModule::class])
    abstract fun bindContactActivity(): ContactsActivity
}
