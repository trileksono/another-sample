package com.tleksono.base.di

import com.tleksono.base.OwnApps
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        AppModule::class,
        ViewModelModule::class,
        ActivityBuilder::class
    ]
)
interface AppComponent : AndroidInjector<OwnApps> {
    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(application: OwnApps): Builder
        fun build(): AppComponent
    }
}
